package ch2;

import java.io.*;
import java.util.Scanner;

public class Circle {
    double  radius;
    //成员变量radius的getter方法
    public double getRadius() {
        return radius;
    }
    //成员变量radius的setter方法
    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {

        return radius * radius * Math.PI;
    }

}
