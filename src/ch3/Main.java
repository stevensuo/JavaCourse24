package ch3;


public class Main {
    public static void main(String[] args) {
        System.out.println(Math.sin(Math.PI/6));
    }
}
class Circle {
    private double radius;
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public double getRadius(){
        return radius;
    }

    public void setRadius(double r){
        radius = r;

    }


    public Circle(){
        radius = 1.0;
    }
}