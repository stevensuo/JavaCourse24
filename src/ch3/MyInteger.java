package ch3;


public class MyInteger {
    int value;
    final int MAX=2147483647;
    void abs(){
        value = Math.abs(value);
    }
    void print(){
        System.out.println(value);
    }
}

class Demo{
    public static void main(String[] args) {
        MyInteger mi1=new MyInteger();
        mi1.value = -3;
        MyInteger mi2 = new MyInteger();
        mi2.value = -3;
        int i = -3;
        int j = i;
        i++;
        System.out.println(i);
        System.out.println(j);
        mi1.print();
        mi2.print();
        mi1.abs();
        mi1.print();
        mi2.print();

        String s1 = "Java";
        String s2 = "Java";
        String s3 = new String("Java");
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(mi1 == mi2);
        System.out.println(s1 == s2);
        System.out.println(s1 == s3);
        System.out.println(s1.equals(s3));
    }
}