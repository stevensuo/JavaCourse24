package ch4;

/**
 * 类BMI，抽象表示一个人的BMI数据
 * 通过这个类去理解static修饰的静态变量的内涵
 */
public class BMI {
    //人的姓名
    private String name;
    //人的身高
    private double height;
    //人的体重
    private double weight;
    //人的BMI值
    private double bmi;
    private static double HIGH_LEVEL = 24.5;
    private static double LOW_LEVEL = 18.5;
    public BMI(String name, double height, double weight) {
        this.name = name;
        this.height = height;
        this.weight = weight;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public double getHeight() {
        return height;
    }

    public double getWeight() {
        return weight;
    }

    public double getBmi() {
        return bmi;
    }

    public static double getHighLevel() {
        return HIGH_LEVEL;
    }

    public static void setHighLevel(double highLevel) {
        HIGH_LEVEL = highLevel;
    }

    public static double getLowLevel() {
        return LOW_LEVEL;
    }

    public static void setLowLevel(double lowLevel) {
        LOW_LEVEL = lowLevel;
    }

    public double getBMI(){
        bmi = weight / (height* height);
        return  bmi;
    }
    public String getLevel(){
        getBMI();
        if (bmi > HIGH_LEVEL){
            return "Fat";
        }else if(bmi < LOW_LEVEL){
            return "Thin";
        }else{
            return "Normal";
        }
    }
    public String toString(){
        return "name:"+name+",bmi:"+getBMI()+",result:"+getLevel();
    }
}
