package ch4;

/**
 * 使用BMI创建对象的Main类
 */
public class BMIMain {
    public static void main(String[] args) {
        BMI zhangsan = new BMI("张三",1.78,145);
        BMI lisi = new BMI("李四",1.68,85);
        System.out.println(zhangsan.toString());
        System.out.println(lisi.toString());
        //修改static变量的值会影响到该类的所有对象
        BMI.setHighLevel(30.0);

        System.out.println(zhangsan.toString());
        System.out.println(lisi.toString());

    }
}
