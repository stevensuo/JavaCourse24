package ch4;

public class Circle {
    private double radius;


    private String color;
    Circle(){
        radius = 1.0;
        color = "white";
    }
    Circle(double radius){
        this.radius = radius;
    }
    public void setRadius(double radius){
        this.radius = radius;
    }
    public double getRadius(){
        return radius;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Circle(double radius, String color) {
        this.radius = radius;
        this.color = color;
    }

    double getArea(){
        return radius * radius * Math.PI;
    }
}
class Demo{
    public static void main(String[] args) {
        Circle c1 = new Circle(1.6);
        c1.setRadius(2.0);
        System.out.println(c1.getRadius());

    }
}