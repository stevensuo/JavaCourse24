package ch4;

public class Father {
    private String secret = "secret";
    protected double money = 100000000.0;
    String experience = "enjoy life";
    public String name = "zhangsan";

    private String getSecret() {
        return secret;
    }

    protected double getMoney() {
        return money;
    }

    String getExperience() {
        return experience;
    }

    public String getName() {
        return name;
    }
}
