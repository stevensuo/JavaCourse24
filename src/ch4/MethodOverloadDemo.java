package ch4;

public class MethodOverloadDemo {
    public MethodOverloadDemo(){

    }
    public MethodOverloadDemo(int num){

    }
    public void method(){

    }
    int method(int num){
        return 1;
    }
    public void method(long num){

    }
    public void method(String s,int num){}
    public void method(int num,String s){}

    public static void main(String[] args) {
        MethodOverloadDemo demo = new MethodOverloadDemo();
        demo.method();
        demo.method(1);
        demo.method("hello",1);
        demo.method(1,"222");
        System.out.println(1);
        System.out.println(1.1f);
        System.out.println("Hello");
    }
}
