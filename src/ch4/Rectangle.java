package ch4;

/**
 * 表示矩形的类Rectangle，
 */
public class Rectangle {

    String name;
    double length;
    double width;
    //double area;
   public Rectangle(String n,double l,double w){
       name = n;
       length = l;
       width = w;
   }
   public Rectangle(String n){
       name = n;
   }
    public double getArea(){
        return length * width;
    }

    /**
     * 将该方法与上面的构造器对比思考，两者的相同点与不同点
     * @param n 矩形的名字
     * @param l 矩形的长
     * @param w 矩形的宽
     */
    public void init(String n,double l,double w){
        name = n;
        length = l;
        width = w;
    }
    public String toString(){
        return "rectangle's name:"+name+",length:"+length+",width:"+width;
    }
}
