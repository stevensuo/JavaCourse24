package ch4;

/**
 * static关键字的一些用法
 */
public class StaticDemo {
    //static修饰的成员变量为静态变量/类变量，该类对象共享一个静态变量
    //可以在静态方法或实例方法中访问
    static int staticNum;
    //没有static修饰的成员变量为实例变量，每个对象都有一个实例变量的副本
    //实例变量不能在静态方法或静态代码块中直接访问
    int num = 1;
    //static：静态代码块，在类装载后就直接运行
    static {
        staticNum = 1;
        System.out.println("static block");
    }
    //static修饰的成员方法为静态方法
    //静态方法,在对象生成之前就可以用
    //静态方法中可以直接访问静态变量，不能访问实例变量
    static void staticMethod(){
        System.out.println(staticNum);
        //System.out.println(num);
    }
   //实例方法，必须在对象生成后，通过对象的引用来调用
   void instanceMethod(){
        System.out.println(staticNum);
        System.out.println(num);
    }

}
