package ch4;

/**
 * StuScore类表示一条学生成绩数据
 */
public class StuScore {
    //静态代码块，初始化PASS_LINE
    static {
        PASS_LINE = 60.0;
        System.out.println("This is a static block");
    }
    //学生姓名
    private String name;
    //学生学号
    private String id;
    //学生的分数
    private double score;
    private static double PASS_LINE;

    public static void setPassLine(double passLine) {

        StuScore.PASS_LINE = passLine;
    }

    public StuScore(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public StuScore(String name, String id, double score) {
        //使用this关键字调用重载的构造器，记住，该语句一定是该构造器的第一条语句
        this(name,id);
        this.score = score;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public static double getPassLine() {
        return PASS_LINE;
    }

    public void printLevel(){
        if (score >= PASS_LINE){
            System.out.println("Passed");
        }else{
            System.out.println("Unpassed");
        }
    }
}
