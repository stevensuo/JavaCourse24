package ch5;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateDemo {
    public static void main(String[] args) {
        Date date = new Date();
        System.out.println(date.getTime());
        date.setTime(1710991454852L);
        Date date2 = new Date();
        date2.setTime(1710991464852L);
        System.out.println(date.compareTo(date2));
        System.out.println(date2.toString());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        System.out.println(sdf.format(date2));

    }
}
