package ch5;

/**
 * Math类的一些常用方法介绍
 */
public class MathDemo {
    public static void main(String[] args) {
        System.out.println(Math.abs(-100)); // 100
        System.out.println(Math.abs(-7.8)); // 7.8
        System.out.println(Math.max(100, 99)); // 100
        System.out.println(Math.min(1.2, 2.3));//1.2
        System.out.println(Math.pow(2, 10));
        System.out.println(Math.sqrt(2));
        System.out.println(Math.log10(100));
        System.out.println(Math.sin(3.14));
        double pi = Math.PI; // 3.14159...
        double e = Math.E; // 2.7182818...
        System.out.println(Math.sin(Math.PI / 6)); // sin(π/6) = 0.5
        generateRandom();
    }
    public static void generateRandom(){
        double x = Math.random(); // x的范围是[0,1)
        double min = 10;
        double max = 50;
        double y = x * (max - min) + min; // y的范围是[10,50)
        long n = (long) y; // n的范围是[10,50)的整数
        System.out.println(y);
        System.out.println(n);

    }
}
