package ch5;

import java.security.SecureRandom;
import java.util.Random;

public class RandomDemo {
    public static void main(String[] args) {
        Random rand1 = new Random();
        Random rand2 = new Random(1000);
        for (int i = 0; i < 5; i++) {
            System.out.print(rand1.nextInt(10000)+" ");
        }
        System.out.println();
        for (int i = 0; i < 5; i++) {
            System.out.print(rand2.nextInt(10000)+" ");
        }
        System.out.println();
        SecureRandom sr = new SecureRandom();
        for (int i = 0; i < 5; i++) {
            System.out.print(sr.nextInt(10000)+" ");
        }
    }
}
