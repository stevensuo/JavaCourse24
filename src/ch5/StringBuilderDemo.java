package ch5;

public class StringBuilderDemo {
    public static void main(String[] args) {
        StringBuilderDemo demo = new StringBuilderDemo();
        demo.run();
    }
    public void run(){
        stringAdd();
        stringBuilderAdd();
        stringAddLoop();
        stringBuilderAddLoop();
    }
    public void stringAdd(){
        long startTime = System.currentTimeMillis();

        int count = 100000;
        for (int i = 0; i < count; i++) {
            String str = "测试用+号连接字符串的效率:-"+i;
        }
        long endTime = System.currentTimeMillis();
        System.out.println("用+拼接字符串，重复" + count + "次，花费" + (endTime - startTime) + "毫秒");
    }

    public void stringAddLoop(){
        long startTime = System.currentTimeMillis();
        String s = "";
        for (int i = 0; i < 100000; i++) {
            s = s + "," + i;
            //System.out.println(s.getClass().getName() + "@" + Integer.toHexString(s.hashCode()));
        }
        long endTime = System.currentTimeMillis();
        System.out.println("用+循环拼接字符串100000次，花费" + (endTime - startTime) + "毫秒");
    }
    public void stringBuilderAdd(){
        long startTime = System.currentTimeMillis();
        int count = 100000;
        StringBuilder sb = new StringBuilder("测试用+号连接字符串的效率");
        for (int i = 0; i < count; i++) {
            sb.append("-").append(i);
        }
        long endTime = System.currentTimeMillis();
        System.out.println("用StringBuilder拼接字符串，重复" + count + "次，花费" + (endTime - startTime) + "毫秒");
    }
    public void stringBuilderAddLoop(){
        long startTime = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder(1024);
        for (int i = 0; i < 100000; i++) {
            sb.append(',');
            sb.append(i);
            //System.out.println(sb.getClass().getName() + "@" + Integer.toHexString(sb.hashCode()));

        }
        String s = sb.toString();
        long endTime = System.currentTimeMillis();
        System.out.println("用StringBuilder循环拼接字符串，重复100000次，花费" + (endTime - startTime) + "毫秒");
    }
    public void stringBuilderChain(){
        /*var sb = new StringBuilder(1024);
        sb.append("Mr ")
                .append("Bob")
                .append("!")
                .insert(0, "Hello, ");
        System.out.println(sb.toString());*/
    }
}
