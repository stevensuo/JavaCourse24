package ch5;

import java.util.Arrays;

public class StringDemo {
    public static void main(String[] args) {
        StringDemo demo = new StringDemo();
        demo.run();
    }
    public void run(){
       replaceString();
    }

    /**
     * 分割字符串
     */
    public void splitString(){
        String s = "A|B|C|D";
        String[] ss = s.split("\\|"); // {"A", "B", "C", "D"}
        System.out.println(Arrays.toString(ss));
    }

    /**
     * 格式化字符串
     */
    public String formatString(){
        String s = "Hi %s, your score is %d!";
       // System.out.println(s.formatted("Alice", 80));
        System.out.println(String.format("Hi %s, your score is %.2f!", "Bob", 59.5));
       return s;
        // return s.formatted("Alice", 80);
    }
    public void searchString(){
        System.out.println("Hello".indexOf("l")); // 2
        System.out.println("Hello".lastIndexOf("l")); // 3
        System.out.println("Hello".startsWith("He")); // true
        System.out.println("Hello".endsWith("lo")); // true
        System.out.println("Hello".substring(2)); // "llo"
        System.out.println("Hello".substring(2, 4)); //ll
    }
    public void emptyOrBlank(){
        System.out.println("".isEmpty()); // true，因为字符串长度为0
        System.out.println("  ".isEmpty()); // false，因为字符串长度不为0
        //System.out.println("  \n".isBlank()); // true，因为只包含空白字符
        //System.out.println(" Hello ".isBlank()); // false，因为包含非空白字符
    }
    public void replaceString(){
        String s = "hello";
        String s1 = s.replace('l', 'w'); // "hewwo"，所有字符'l'被替换为'w'
        System.out.println(s1);
        String s2 = s.replace("ll", "~~"); // "he~~o"，所有子串"ll"被替换为"~~"
        System.out.println(s2);
    }
}
