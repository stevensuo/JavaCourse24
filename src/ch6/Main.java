package ch6;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        main.run();

    }
    public void run(){
        //methodOverride();
        //upwardObjectAndPolymorphic();
        //overrideMethod();
        useEquals();
    }

    /**
     * 验证子类Override类父类的introduce方法，各个对象的introduce方法输出的结果不一样。
     */
    public void methodOverride(){
        Person person = new Person("张三");
        Student stu = new Student("李四","2023001");
        Teacher teacher = new Teacher("Tom");
        person.introduce();
        stu.introduce();
        teacher.introduce();

    }

    /**
     * 用父类变量person引用一个类对象，比如下面的：Person person = new Student("李四","2023001");就是一个上转型对象，=左边是一个Person类型的变量；
     * 而=右边则是一个Student的对象，这些的称为上转型对象。同理后面的person = new Teacher("Tom");也是类似。
     * 下面出现了两次：person.introduce();代码，但这两行一模一样的代码输出的结果却是不同的，这就是多态的一种体现。
     */
    public void upwardObjectAndPolymorphic(){
        Person person = new Student("李四","2023001");
        person.introduce();
        person = new Teacher("Tom");
        person.introduce();
        //person.work();
    }
    public void overrideMethod(){
        Teacher teacher = new Teacher("Tom");
        System.out.println(teacher);
    }
    public void useEquals(){
        Student stu1 = new Student("zhangsan","001");
        Student stu2 = new Student("zhangsan","001");
        System.out.println(stu1 == stu2);
        System.out.println(stu1.equals(stu2));
        System.out.println(stu1.hashCode());
        System.out.println(stu2.hashCode());
    }
}
