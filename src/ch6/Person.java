package ch6;

public class Person {
    private String name;

    /**
     * 构造器，初始化姓名name。
     *
     * @param name
     */
    public Person(String name) {
        System.out.println("create a Person instance");
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void introduce(){
        System.out.println("I am "+name);
    }

}
