package ch6;

import ch6.Person;

/**
 * 定义Student类，该类继承于Person类
 */
public class Student extends Person {
    private String stuId;

    public Student(String name,String stuId) {
        //用super关键字显示调用父类的构造器，初始化name变量，该代码必须是构造器的第一行代码
        super(name);
        this.stuId = stuId;
    }
    @Override
    public boolean equals(Object obj) {
        Student stu = (Student) obj;
        return getName().equals(stu.getName()) && stuId.equals(stu.getStuId());
    }


    public String getStuId() {
        return stuId;
    }

    public void setStuId(String stuId) {
        this.stuId = stuId;
    }
//注解

    /**
     * Student类覆盖Person类的introduce()方法
     */
    @Override
    public void introduce(){
        System.out.println("My name is: "+getName()+",My ID is:"+stuId);
    }

    /**
     * 重载introduce方法
     * @param stuId 学生的学号
     */
    public void introduce(String stuId){
        System.out.println("My ID is:"+stuId);
    }

}
