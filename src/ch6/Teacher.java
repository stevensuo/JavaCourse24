package ch6;

public class Teacher extends Person{
    private String name;
    /**
     * Teacher作为Person的子类，在其构造器中必须要调用父类的构造器。子类在构造器中调用父类构造器分为两种情况：
     * 1、隐式调用：就是说不用写调用代码，编译器自动调用父类构造器，但前提是只能调用父类无参数的构造器。如果父类没有无参数的构造器，则无法隐式调用；
     * 2、显示调用：父类的所有构造器都是带参数的情况下，子类在其构造器中必须用super显示调用父类的构造器
     * @param name
     */
    public Teacher(String name){
        //显示调用父类带参数的构造器
        super(name);
    }
    @Override
    public void introduce(){
        System.out.println("I am a Teacher ,My name is: "+getName());
    }

    @Override
    public String toString(){

        return "This is a Teacher,his name:";
    }
    public void work(){
        System.out.println("上课");
    }
}

