package ch7;

public class Bonus extends Income {


    public Bonus(String month, double money) {
        super(month,money);
    }
    @Override
    public double getTax(){
        return getMoney() * 0.1;
    }
}
