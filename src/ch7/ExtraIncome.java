package ch7;

public class ExtraIncome extends Income{


    public ExtraIncome(String month, double money) {
        super(month,money);
    }

    @Override
    public double getTax() {
        return getMoney() * 0.5;
    }

}
