package ch7;

/**
 * 因为包含抽象方法，所以Income类声明为抽象类
 */
public abstract class Income {
    private String month;
    private double money;

    public Income(String month, double money) {
        this.month = month;
        this.money = money;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    /**
     * 抽象方法
     * @return 返回计算的税
     */
    public abstract double getTax();
}
