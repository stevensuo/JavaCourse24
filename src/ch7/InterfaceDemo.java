package ch7;

public class InterfaceDemo {
    public static void main(String[] args) {
        //匿名类
        Runner runner = new Runner() {
            @Override
            public void start() {

            }

            @Override
            public void run() {

            }

            @Override
            public void stop() {

            }
        };
        Runner tiger = new Tiger();
        Runner car = new Car();
    }
}
