package ch7;

public class PersonIncome {
    private String name;
    private Income[] incomes;

    public PersonIncome(String name, Income... incomes) {
        this.name = name;
        this.incomes = incomes;
    }
    public double getTotalTax(){
        double total = 0.0;
        for (Income income:incomes){
            total += income.getTax();
        }
        return total;
    }
}
