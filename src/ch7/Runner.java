package ch7;

public interface Runner extends BaseInterface,SuperInterface {
    public static final int speed = 0;
    void start();
    void run();
    void stop();
    default void test(){

    }
}
