package ch7;

public class Salary extends Income{


    public Salary(String month, double money) {
        super(month,money);
    }
    @Override
    public double getTax(){
        if (getMoney() < 5000){
            return 0.0;
        }else{
            return (getMoney() - 5000) * 0.03;
        }

    }
}
