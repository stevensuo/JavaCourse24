package ch7;

public class TaxDemo {
    public static void main(String[] args) {
        //Income income = new Income("3月",10000);
        Salary salary = new Salary("3月",7000);
        Bonus bonus = new Bonus("3月",5000);
        ExtraIncome extraIncome = new ExtraIncome("3月",10000);
        //Income[] incomes = {salary,bonus};
        PersonIncome personIncome = new PersonIncome("Tom",salary,bonus,extraIncome);
        System.out.println(personIncome.getTotalTax());
    }
}
