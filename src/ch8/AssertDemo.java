package ch8;

import java.util.Scanner;

public class AssertDemo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int num = scanner.nextInt();
        assert num > 0 : "error input";
    }
}
