package ch8;

public class ConnectionDemo {
    public static void main(String[] args) {
       process2();

    }
    public static void process1(){
        NetConnection connection = new NetConnection();
        try {
            connection.transfer();
            System.out.println("transfer complete");
        } catch (Exception e) {
            System.out.println("error");
            throw new RuntimeException(e);
        }finally {
            try {
                connection.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            //System.out.println("close connection");

        }
    }
    public static void process2(){
        try (NetConnection connection = new NetConnection()){
            connection.transfer();
            System.out.println("transfer complete");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}
