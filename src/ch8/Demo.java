package ch8;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Demo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        try {
            int d1 = scanner.nextInt();
            int  d2 = scanner.nextInt();
            int result = divide(d1,d2);
            System.out.println(d1 + "/"+d2+" = "+ result);

        }catch (ArithmeticException e){
            System.out.println("除数不能为0");
        }catch (InputMismatchException e){
            System.out.println("请输入整数");
        }catch (Exception e){

        }
        finally {
            System.out.println("done!");
        }




    }
    public static int divide(int d1,int d2){

        return d1/d2;
    }
}
