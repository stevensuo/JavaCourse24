package ch8;

public class NetConnection implements AutoCloseable{
    public NetConnection(){
        System.out.println("Connected");
    }
    public void transfer() throws Exception{
        System.out.println("transfer data");
        throw new Exception("transfer error");
    }
    @Override
    public void close() throws Exception {
        System.out.println("connection closed");
        throw new Exception("close error");
    }
}
