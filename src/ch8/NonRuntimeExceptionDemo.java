package ch8;

import ch3.Main;

public class NonRuntimeExceptionDemo {
    public static void main(String[] args) {
        try {
            process1();
        } catch (MyException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
    public static void process1() throws MyException {
        process2();
    }



    public static void process2() throws MyException{
        double r = Math.random();
        if (r < 0.5){
            MyException e = new MyException("出错了");
            throw e;
        }
    }
}
