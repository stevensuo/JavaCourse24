package temp;

public class Rectangle {
    private double width;
    private double length;
    public Rectangle(){
        width = 1.0;
        length = 1.0;
    }
    public Rectangle(double width){
        this.width = width;
    }
    public Rectangle(double width,double length){
        this(width);
        this.length = length;
    }

    //getter
    public double getWidth() {
        return this.width;
    }

    public double getLength() {
        return length;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea(){
        return width * length;
    }
}
